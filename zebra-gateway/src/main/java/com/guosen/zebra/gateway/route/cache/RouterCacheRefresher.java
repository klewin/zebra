package com.guosen.zebra.gateway.route.cache;

import com.google.common.collect.Sets;
import com.guosen.zebra.gateway.dao.RouteConfigDao;
import com.guosen.zebra.gateway.route.model.RouteConfig;
import com.guosen.zebra.gateway.route.model.RouteInfo;
import com.guosen.zebra.gateway.route.parser.ConcreteUrlRouteInfoParser;
import com.guosen.zebra.gateway.route.parser.UrlPrefixRouteInfoParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 路由信息更新器
 */
@Component
public class RouterCacheRefresher {
    private static final Logger LOGGER = LoggerFactory.getLogger(RouterCacheRefresher.class);

    @Autowired
    private RouteConfigDao routeConfigDao;


    public void refresh() {

        LOGGER.info("Begin to refresh whole cache.");

        List<RouteConfig> routeConfigs = routeConfigDao.getConfigs();
        if (routeConfigs == null) {
            routeConfigs = Collections.emptyList();
        }

        Set<String> latestMicroServiceNames = routeConfigs.stream()
                .map(RouteConfig::getServiceName)
                .collect(Collectors.toSet());

        // 和已有的对比
        Set<String> oldMicroServiceNames = RouteConfigCache.getInstance().getOldConfigs()
                .stream()
                .map(RouteConfig::getServiceName)
                .collect(Collectors.toSet());

        Set<String> deletedMicroServiceNames = Sets.difference(oldMicroServiceNames, latestMicroServiceNames);

        handleRouteInfoDeleted(deletedMicroServiceNames);

        for (RouteConfig routeConfig : routeConfigs) {
            handleRouteInfoChange(routeConfig);
        }

        RouteConfigCache.getInstance().setOldConfigs(routeConfigs);

        LOGGER.info("Finish to refresh whole cache.");
    }

    /**
     * 处理路由信息变更事件
     * @param routeConfig   变更的路由配置
     */
    private void handleRouteInfoChange(RouteConfig routeConfig) {
        String microServiceName = routeConfig.getServiceName();

        LOGGER.info("Begin refresh micro service {} route cache.", microServiceName);

        Map<String, RouteInfo> urlRouteInfoMapOfMicroService = ConcreteUrlRouteInfoParser.parse(routeConfig);
        Map<String, RouteInfo> urlPrefixRouteInfoMapOfMicroservice = UrlPrefixRouteInfoParser.parse(routeConfig);

        ConcreteUrlRouteCache.getInstance().update(microServiceName, urlRouteInfoMapOfMicroService);
        UrlPrefixRouteCache.getInstance().update(microServiceName, urlPrefixRouteInfoMapOfMicroservice);

        LOGGER.info("Finish refresh micro service {} route cache.", microServiceName);
    }

    private void handleRouteInfoDeleted(Set<String> deletedMicroServiceNames) {
        if (CollectionUtils.isEmpty(deletedMicroServiceNames)) {
            return;
        }

        for (String deletedMicroServiceName : deletedMicroServiceNames) {
            ConcreteUrlRouteCache.getInstance().update(deletedMicroServiceName, Collections.emptyMap());
            UrlPrefixRouteCache.getInstance().update(deletedMicroServiceName, Collections.emptyMap());
        }
    }

}
