package com.guosen.zebra.gateway.service;

import com.guosen.zebra.core.common.ZebraConstants;
import com.guosen.zebra.core.grpc.anotation.ZebraService;

/**
 * 网关服务定义，仅仅为了注册，不提供任何 gRPC 定义
 */
@ZebraService(service = "null", type = ZebraConstants.TYPE_GATEWATY)
public class GatewayService {
}
